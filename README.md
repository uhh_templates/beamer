This is an unofficial Latex Beamer Template based on the corporate
design of Universität Hamburg.

Authors: Christine Köhn and Fabian Barteld

Installation Requirements
-------------------------

You need to download the university logo and the fonts from the intranet and put them directly in the beamer (or whatever name you assigned) folder:

- [The logo (for standard and print) in color as PDF](https://www.intranet.uni-hamburg.de/themen/oeffentlichkeitsarbeit/corporate-design/logo/downloads/up-uhh-logo-u-2010-u-farbe-u-cmyk.pdf): Its name should be `up-uhh-logo-u-2010-u-farbe-u-cmyk.pdf`

- [The university fonts](http://online-dienste.verwaltung.uni-hamburg.de/projekte/fonts/lizenzvereinbarung.html): Save them as:
  - `TheSansUHH-Regular.ttf`
  - `TheSansUHH-Regular-Italic.ttf`
  - `TheSansUHH-Bold-Italic.ttf`
  - `TheSansUHH-Bold.ttf`
  - `TheSansUHH-BoldCaps.ttf`
  - `TheSansUHH-SemiLightCaps.ttf`

Compiling
---------

Compile your presentation with lualatex:

```
lualatex uhh-example.tex
```

Contributing
------------

If you make changes to the template that might be useful to other people as well, please share your work:

https://gitlab.com/uhh_templates/beamer
